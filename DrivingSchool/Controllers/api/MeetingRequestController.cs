﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using DrivingSchool.Dtos;
using DrivingSchool.Models;
using System.Data.Entity;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace DrivingSchool.Controllers.api
{
    public class MeetingRequestController : ApiController
    {
        //initialize database
        private ApplicationDbContext _context;

        public MeetingRequestController()
        {
            _context=new ApplicationDbContext();
        }

        //GET /api/MeetingRequest
        //public IEnumerable<RequestAMeeting> GetMeetings()
        //{
        //    return _context.RequestAMeetings.ToList();
        //}
        //after automapping
        //public IEnumerable<RequestAMeetingDto> GetMeetings()
        //{
        //    return _context.RequestAMeetings.ToList()
        //        .Select(Mapper.Map<RequestAMeeting, RequestAMeetingDto>); //removed parenthesis coz we dont want it be executed

        //}
        public IHttpActionResult GetMeetings()
        {
            //var meetingDtos = _context.RequestAMeetings
            //    .Include(c => c.RequestAMeetingFor)
            //    .ToList()
            //    .Select(meeting =>
            //    {
            //        if (meeting == null) throw new ArgumentNullException("meeting");
            //        return Mapper.Map<RequestAMeeting, RequestAMeetingDto>(meeting);
            //    });
                            
            //return Ok(meetingDtos);
            var meetingDtos = _context.RequestAMeetings
                .Include(c => c.RequestAMeetingFor)
                .ToList()
                .Select(Mapper.Map<RequestAMeeting, RequestAMeetingDto>);
            return Ok(meetingDtos);
        }

        //GET /api/MeetingRequest/1
        public IHttpActionResult GetAMeeting(int id)//changed from RequestAMeeting to RequestAMeetingDto and then to IHttpActionResult
        {

            var meetings = _context.RequestAMeetings
                            .Include(c=>c.RequestAMeetingFor)
                            .SingleOrDefault(c => c.Id == id);
            if (meetings == null)
            {
                //throw new HttpResponseException(HttpStatusCode.NotFound);
                return NotFound();
            }
            else
            {
                // return meetings;
                //return Mapper.Map<RequestAMeeting, RequestAMeetingDto>(meetings);
                return Ok(Mapper.Map<RequestAMeeting, RequestAMeetingDto>(meetings));
            }
        }

        

        //DELETE /api/meetingrequest/1
      [System.Web.Http.HttpDelete]
        public void DeleteAMeetingRequest(int id)
        {
            var meetingInDb = _context.RequestAMeetings.SingleOrDefault(c => c.Id == id);
            if(meetingInDb==null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            _context.RequestAMeetings.Remove(meetingInDb);
            _context.SaveChanges();
        }



    }
}
