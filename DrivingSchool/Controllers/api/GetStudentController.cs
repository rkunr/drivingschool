﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;

using System.Data.Entity;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Http;
using System.Web.Services;
using AutoMapper;
using DrivingSchool.Dtos;
using DrivingSchool.Models;

namespace DrivingSchool.Controllers.api
{
    public class GetStudentController : ApiController
    {
        private ApplicationDbContext _context;

        public GetStudentController()
        {
            _context= new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        //public IHttpActionResult GetStudents()
        //{
        //    var studentDto = _context.Students.ToList().Select(Mapper.Map<Student, StudentDto>);
        //    return Ok(studentDto);
        //}

        public IHttpActionResult GetStudents()
        {
            //return _context.Students.ToList().Select(Mapper.Map<Student, StudentDto>);
            var studentDto =
                _context.Students.Include(c => c.StudentJourney).ToList().Select(Mapper.Map<Student, StudentDto>);
            return Ok(studentDto);
        }

        [System.Web.Http.HttpDelete]
        public void DeleteStudents(int id)
        {
            var studentInDb = _context.Students.SingleOrDefault(c => c.Id == id);
            if(studentInDb==null)
                throw  new HttpResponseException(HttpStatusCode.NotFound);
            _context.Students.Remove(studentInDb);
            _context.SaveChanges();

        }





    }//end of controller
}
