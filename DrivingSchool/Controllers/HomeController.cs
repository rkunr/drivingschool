﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DrivingSchool.Models;
using DrivingSchool.ViewModel;

namespace DrivingSchool.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;

        public HomeController()
        {
            _context=new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult Index()
        {
            //return View(User.IsInRole("DrivingTeacher") ? "Admin" : "Index");
            return View();
        }

        public ActionResult Admin()
        {
            return View();
        }

        public ActionResult TolkClient()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ViewResult Tolk()
        {
            
            return View();
        }

        public ActionResult Details(int id)
        {
            var meeting = _context.RequestAMeetings.Include(c => c.RequestAMeetingFor).SingleOrDefault(c => c.Id == id);
            return View(meeting);
        }
        

        

        public ActionResult RequestABooking()
        {
            var bookingType = _context.RequestAMeetingFors.ToList();
            var viewModel = new RequestAMeetingFormViewModel
            {
                RequestAMeeting = new RequestAMeeting(),
                RequestAMeetingFors = bookingType
            };

            return View("RequestABooking",viewModel);
        }


       [HttpPost]
       [ValidateAntiForgeryToken]
       public ActionResult RequestABooking(RequestAMeeting requestAMeeting)
       {
           if (!ModelState.IsValid)
           {
               var viewModel = new RequestAMeetingFormViewModel
               {
                   RequestAMeeting = requestAMeeting,
                   RequestAMeetingFors = _context.RequestAMeetingFors.ToList()
               };
               return View("RequestABooking", viewModel);
           }
           if (requestAMeeting.Id == 0)
               _context.RequestAMeetings.Add(requestAMeeting); //added to the context but not dbase
           else
           {
               var bookingInDb = _context.RequestAMeetings.Single(c => c.Id == requestAMeeting.Id);
               bookingInDb.Subject = requestAMeeting.Subject;
               bookingInDb.RequestAMeetingForId = requestAMeeting.RequestAMeetingForId;
               bookingInDb.FullName = requestAMeeting.FullName;
               bookingInDb.MobileNumber = requestAMeeting.MobileNumber;
               bookingInDb.EmailAddress = requestAMeeting.EmailAddress;
               bookingInDb.Message = requestAMeeting.Message;

           }
           try
           {
               _context.SaveChanges();//save it to the database

               string regarding = "Driving";
               int tolkordriving = requestAMeeting.RequestAMeetingForId;
               if (tolkordriving == 2)
               {
                   regarding = "Tolk";
               }

               var body = "<p>Email From: {0} ({1}) </p><p>Regarding: {2}</p><p>Subject: {3}</p><p>Message: {4}</p>";
               var message = new MailMessage();
               message.To.Add(new MailAddress("rameshkunwar2037@gmail.com"));
               message.Subject = requestAMeeting.Subject;
               message.Body = string.Format(body, requestAMeeting.FullName, requestAMeeting.EmailAddress, 
                   requestAMeeting.Subject, regarding, requestAMeeting.Message);
               message.IsBodyHtml = true;
               using (var smtp = new SmtpClient())
               {
                   //smtp.SendMailAsync(message);
                   smtp.Send(message);
                   //return RedirectToAction("Index", "Home");

               }



           }
           catch (DbEntityValidationException ex)
           {

               foreach (var entityValidationErrors in ex.EntityValidationErrors)
               {
                   foreach (var validationError in entityValidationErrors.ValidationErrors)
                   {
                       System.Diagnostics.Debug.WriteLine("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                   }
               }
           }


           //_context.SaveChanges();
           return RedirectToAction("Index", "Home");
       }

    }

    
}