﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using DrivingSchool.Models;
using DrivingSchool.ViewModel;

namespace DrivingSchool.Controllers
{
    public class StudentController : Controller
    {
        private ApplicationDbContext _context;

        public StudentController()
        {
            _context=new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult Register()
        {
            
            var model = new Student();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(Student student)
        {
            if (!ModelState.IsValid) //give back register form
            {
                
                var model = new Student();
                return View(model);
            }
            if (student.Id == 0)
            {
                _context.Students.Add(student); //added to the context not to database
                _context.SaveChanges();
            }
           
            
          
            return RedirectToAction("Index","Home");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Student student)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new StudentEditFormViewModel
                {
                    Student = student,
                    StudentJourneys = _context.StudentJourneys.ToList()
                };
                return View("Edit", viewModel);
            }
            if (student.Id == 0)
            {
                _context.Students.Add(student); //added to the context not to database
                
            }
            else
            {
                var customerInDb = _context.Students.Single(c => c.Id == student.Id);
                customerInDb.FirstName = student.FirstName;
                customerInDb.MiddleName = student.MiddleName;
                customerInDb.LastName = student.LastName;
                customerInDb.Email = student.Email;
                customerInDb.Mobile = student.Mobile;
                customerInDb.RegistrationAccessCode = student.RegistrationAccessCode;
                customerInDb.BirthDate = student.BirthDate;
                customerInDb.StartDate = student.StartDate;
                //customerInDb.StuStatus = student.StudentJourney.Status;
                customerInDb.StudentJourneyId = student.StudentJourneyId;
            }
            _context.SaveChanges();
            return RedirectToAction("Students","Student");

        }


        public ActionResult Edit(int id)
        {
            var student = _context.Students.SingleOrDefault(c => c.Id == id);
            if (student == null)
                return HttpNotFound();
            var viewModel = new StudentEditFormViewModel
            {
                Student = student,
                StudentJourneys = _context.StudentJourneys.ToList()
            };
            return View("Edit", viewModel);

        }

        public ActionResult Students()
        {
            return View();
        }

        public ActionResult New()
        {
            return View();
        }

        public ActionResult Theory()
        {
            return View();
        }
        public ActionResult Practical ()
        {
            return View();
        }
        public ActionResult Incomplete()
        {
            return View();
        }

        //public ActionResult Edit()
        //{
        //    return View();
        //}



        public ActionResult Details(int id)
        {
            var student = _context.Students.Include(c => c.StudentJourney).SingleOrDefault(c => c.Id == id);
            if (student == null)
                return HttpNotFound();
               
            return View(student);
        }

        //public ActionResult Details(int id)
        //{
        //    var details = _context.StudentJourneys.ToList();
        //    var viewModel = new StudentEditFormViewModel
        //    {
        //        Student = new Student(),
        //        StudentJourneys = details
        //    };
        //    return View("Details", viewModel);
        //}

    }
}