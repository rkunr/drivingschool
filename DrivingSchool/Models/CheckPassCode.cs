﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.EnterpriseServices;
using System.Linq;
using System.Web;

namespace DrivingSchool.Models
{
    public class CheckPassCode: ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var originalCode = (Student) validationContext.ObjectInstance;
            const string anotherCode = "AJ83302";
            //if (originalCode.PassCode==originalCode.Passcode.VerificationCode)
            //{

            //    return ValidationResult.Success;
            //}
            


            if(originalCode.RegistrationAccessCode==null)
                return  new ValidationResult("Verification Pass Code is required");

            return (originalCode.RegistrationAccessCode == anotherCode)
                ? ValidationResult.Success
                : new ValidationResult("Verification code did not match, please ask your teacher");
            
        }
    }
}