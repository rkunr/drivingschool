﻿using System.ComponentModel.DataAnnotations;
using Message = System.Web.Services.Description.Message;

namespace DrivingSchool.Models
{
    public class RequestAMeeting
    {
        public int Id { get; set; }
       
        [Display(Name = "Skrive et emne - Choose a subject")]
        
        public string Subject { get; set; }

        [Required(ErrorMessage = "Error:Please enter your Full Name")]
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [Display(Name = "Mobile Number")]
        public int? MobileNumber { get; set; }
        
        [Required]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Error: Please write a message - Skriv venligst din besked her")]
        [Display(Name = "Skriv venligst din besked her - Please write your message her")]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }

        public RequestAMeetingFor RequestAMeetingFor { get; set; }

        [Required(ErrorMessage = "Error:You must choose Driving or Tolk")]
        [Display(Name = "Choose Driving or Tolk for translation service")]
        public int RequestAMeetingForId { get; set; }
    }
}