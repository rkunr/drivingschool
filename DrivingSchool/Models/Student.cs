﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DrivingSchool.Models
{
    public class Student
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please provide the First Name")]
        [Display(Name = "Frist Name")]
        public string FirstName { get; set; }

       [Display(Name ="Middle Name")]
        public string MiddleName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        
        [Required]
        public int? Mobile { get; set; }
        public DateTime? BirthDate { get; set; }

        
        [Display(Name = "Date of Start")]
        [Required(ErrorMessage = "Please enter the start date of your course")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        public Student()
        {
            Id = 0;
            StudentJourneyId = 1;
            StuStatus = "New"; //initializing the studentstatus by new this can be done in easier way in C#6
            //public string StudentStatus{get; set;}="New";
        }

        public string StuStatus { get; set; }


        [Display(Name = "Registration Code")]
        [CheckPassCode]
        public string RegistrationAccessCode { get; set; }//Students need to enter a passcode give by the teacher

        public StudentJourney StudentJourney { get; set; }
        public int? StudentJourneyId { get; set; }
        
        
        

        
    }
}