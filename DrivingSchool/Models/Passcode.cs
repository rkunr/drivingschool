﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace DrivingSchool.Models
{
    public class Passcode
    {
        public int Id { get; set; }
        public string VerificationCode { get; set; }
    }
}