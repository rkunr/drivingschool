﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DrivingSchool.Models;

namespace DrivingSchool.Dtos
{
    public class RequestAMeetingDto
    {
        public int Id { get; set; }

        public string Subject { get; set; }

        [Required]
        public string FullName { get; set; }

        public int MobileNumber { get; set; }

        [Required]
        public string EmailAddress { get; set; }

        [Required]
        
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }

        public  RequestAMeetingForDto RequestAMeetingFor { get; set; }

       public int RequestAMeetingForId { get; set; }
    }
}