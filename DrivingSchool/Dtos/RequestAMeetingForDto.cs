﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DrivingSchool.Dtos
{
    public class RequestAMeetingForDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}