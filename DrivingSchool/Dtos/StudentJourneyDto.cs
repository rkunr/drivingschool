﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DrivingSchool.Dtos
{
    public class StudentJourneyDto
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}