﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DrivingSchool.Models;

namespace DrivingSchool.Dtos
{
    public class StudentDto
    {
        public int Id { get; set; }

        
        [Required]
        public string FirstName { get; set; }

       
        public string MiddleName { get; set; }

        
        [Required]
        public string LastName { get; set; }
        
       
        [Required]
        public string Email { get; set; }
        
        [Required]
        public int? Mobile { get; set; }
        public DateTime? BirthDate { get; set; }

       
        [Required]
        public DateTime? StartDate { get; set; }

        public StudentDto()
        {
            Id = 0;
            StuStatus = "New"; //initializing the studentstatus by new this can be done in easier way in C#6
            //public string StudentStatus{get; set;}="New";
        }

        public string StuStatus { get; set; }

        [Required]
       public string RegistrationAccessCode { get; set; }//Students need to enter a passcode give by the teacher

        public StudentJourneyDto StudentJourney { get; set; }
        public int? StudentJourneyId { get; set; }
    }
}