﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using DrivingSchool.Dtos;
using DrivingSchool.Models;

namespace DrivingSchool.App_Start
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<RequestAMeeting, RequestAMeetingDto>();
            Mapper.CreateMap<RequestAMeetingDto, RequestAMeeting>();
            Mapper.CreateMap<RequestAMeetingFor, RequestAMeetingForDto>();
            Mapper.CreateMap<RequestAMeetingForDto, RequestAMeetingFor>();
            Mapper.CreateMap<Student, StudentDto>();
            Mapper.CreateMap<StudentDto, Student>();
            Mapper.CreateMap<StudentJourney, StudentJourneyDto>();
            Mapper.CreateMap<StudentJourneyDto, StudentJourney>();

        }

    }
}