﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DrivingSchool.Startup))]
namespace DrivingSchool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
