namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MadeStudentForeignKeyNullable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Students", "StudentJourneyId", "dbo.StudentJourneys");
            DropIndex("dbo.Students", new[] { "StudentJourneyId" });
            AlterColumn("dbo.Students", "StudentJourneyId", c => c.Int());
            CreateIndex("dbo.Students", "StudentJourneyId");
            AddForeignKey("dbo.Students", "StudentJourneyId", "dbo.StudentJourneys", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "StudentJourneyId", "dbo.StudentJourneys");
            DropIndex("dbo.Students", new[] { "StudentJourneyId" });
            AlterColumn("dbo.Students", "StudentJourneyId", c => c.Int(nullable: false));
            CreateIndex("dbo.Students", "StudentJourneyId");
            AddForeignKey("dbo.Students", "StudentJourneyId", "dbo.StudentJourneys", "Id", cascadeDelete: true);
        }
    }
}
