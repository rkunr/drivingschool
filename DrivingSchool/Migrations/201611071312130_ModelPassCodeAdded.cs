namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelPassCodeAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Passcodes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VerificationCode = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Passcodes");
        }
    }
}
