namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulatePassCode : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO PassCodes(VerificationCode) VALUES ('AJ83302')");
        }
        
        public override void Down()
        {
        }
    }
}
