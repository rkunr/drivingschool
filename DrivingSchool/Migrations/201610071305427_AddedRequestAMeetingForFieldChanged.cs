namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRequestAMeetingForFieldChanged : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RequestAMeetingFors", "Name", c => c.String());
            DropColumn("dbo.RequestAMeetingFors", "Driving");
            DropColumn("dbo.RequestAMeetingFors", "Tolk");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RequestAMeetingFors", "Tolk", c => c.String());
            AddColumn("dbo.RequestAMeetingFors", "Driving", c => c.String());
            DropColumn("dbo.RequestAMeetingFors", "Name");
        }
    }
}
