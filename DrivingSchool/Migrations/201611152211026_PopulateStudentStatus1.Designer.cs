// <auto-generated />
namespace DrivingSchool.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class PopulateStudentStatus1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(PopulateStudentStatus1));
        
        string IMigrationMetadata.Id
        {
            get { return "201611152211026_PopulateStudentStatus1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
