namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelStudentStatusModified : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentStatus", "Status", c => c.String());
            DropColumn("dbo.StudentStatus", "New");
            DropColumn("dbo.StudentStatus", "Theory");
            DropColumn("dbo.StudentStatus", "Practical");
            DropColumn("dbo.StudentStatus", "Incomplete");
        }
        
        public override void Down()
        {
            AddColumn("dbo.StudentStatus", "Incomplete", c => c.String());
            AddColumn("dbo.StudentStatus", "Practical", c => c.String());
            AddColumn("dbo.StudentStatus", "Theory", c => c.String());
            AddColumn("dbo.StudentStatus", "New", c => c.String());
            DropColumn("dbo.StudentStatus", "Status");
        }
    }
}
