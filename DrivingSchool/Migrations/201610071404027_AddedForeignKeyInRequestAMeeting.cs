namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedForeignKeyInRequestAMeeting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RequestAMeetings", "RequestAMeetingForId", c => c.Int(nullable: false));
            CreateIndex("dbo.RequestAMeetings", "RequestAMeetingForId");
            AddForeignKey("dbo.RequestAMeetings", "RequestAMeetingForId", "dbo.RequestAMeetingFors", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RequestAMeetings", "RequestAMeetingForId", "dbo.RequestAMeetingFors");
            DropIndex("dbo.RequestAMeetings", new[] { "RequestAMeetingForId" });
            DropColumn("dbo.RequestAMeetings", "RequestAMeetingForId");
        }
    }
}
