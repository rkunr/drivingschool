namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateAddedRequestAMeetingFors : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO  RequestAMeetingFors(Name) VALUES ('Driving')");
            Sql("INSERT INTO  RequestAMeetingFors(Name) VALUES ('Tolk')");
        }
        
        public override void Down()
        {
        }
    }
}
