namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelNameRegardingsPropertyChanged : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RequestAMeetings", "Regarding", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RequestAMeetings", "Regarding", c => c.String(nullable: false));
        }
    }
}
