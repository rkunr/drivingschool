// <auto-generated />
namespace DrivingSchool.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ModelStudentModified : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ModelStudentModified));
        
        string IMigrationMetadata.Id
        {
            get { return "201611161458548_ModelStudentModified"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
