namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeignKeyAddedInStudentModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "StudentJourneyId", c => c.Int(nullable: true));
            CreateIndex("dbo.Students", "StudentJourneyId");
            AddForeignKey("dbo.Students", "StudentJourneyId", "dbo.StudentJourneys", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "StudentJourneyId", "dbo.StudentJourneys");
            DropIndex("dbo.Students", new[] { "StudentJourneyId" });
            DropColumn("dbo.Students", "StudentJourneyId");
        }
    }
}
