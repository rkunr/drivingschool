namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ViewModelStudentRegisterAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "PasscodeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Students", "PasscodeId");
            AddForeignKey("dbo.Students", "PasscodeId", "dbo.Passcodes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "PasscodeId", "dbo.Passcodes");
            DropIndex("dbo.Students", new[] { "PasscodeId" });
            DropColumn("dbo.Students", "PasscodeId");
        }
    }
}
