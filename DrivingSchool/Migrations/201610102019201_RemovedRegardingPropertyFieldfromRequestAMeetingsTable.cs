namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedRegardingPropertyFieldfromRequestAMeetingsTable : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.RequestAMeetings", "Regarding");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RequestAMeetings", "Regarding", c => c.String());
        }
    }
}
