namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateStudentStatus1 : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO StudentStatus(Status) VALUES ('New')");
            Sql("INSERT INTO StudentStatus(Status) VALUES ('Theory')");
            Sql("INSERT INTO StudentStatus(Status) VALUES ('Practical')");
            Sql("INSERT INTO StudentStatus(Status) VALUES ('Incomplete')");
        }
        
        public override void Down()
        {
        }
    }
}
