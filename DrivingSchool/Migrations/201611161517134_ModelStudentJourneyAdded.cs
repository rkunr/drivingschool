namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelStudentJourneyAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StudentJourneys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.StudentJourneys");
        }
    }
}
