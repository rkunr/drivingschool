namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelStudentStatusDeleted : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.StudentStatus");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.StudentStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
