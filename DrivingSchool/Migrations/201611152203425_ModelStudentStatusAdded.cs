namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelStudentStatusAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StudentStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        New = c.String(),
                        Theory = c.String(),
                        Practical = c.String(),
                        Incomplete = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.StudentStatus");
        }
    }
}
