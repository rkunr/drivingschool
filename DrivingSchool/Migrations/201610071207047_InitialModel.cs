namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RequestAMeetings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Regarding = c.String(),
                        Subject = c.String(),
                        FullName = c.String(),
                        MobileNumber = c.Int(nullable: false),
                        EmailAddress = c.String(),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.RequestAMeetings");
        }
    }
}
