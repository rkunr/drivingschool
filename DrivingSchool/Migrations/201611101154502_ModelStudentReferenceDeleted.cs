namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelStudentReferenceDeleted : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Students", "PasscodeId", "dbo.Passcodes");
            DropIndex("dbo.Students", new[] { "PasscodeId" });
            DropColumn("dbo.Students", "PasscodeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Students", "PasscodeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Students", "PasscodeId");
            AddForeignKey("dbo.Students", "PasscodeId", "dbo.Passcodes", "Id", cascadeDelete: true);
        }
    }
}
