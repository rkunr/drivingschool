namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PassCodeChangedToRegistrationAccessCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "RegistrationAccessCode", c => c.String());
            DropColumn("dbo.Students", "PassCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Students", "PassCode", c => c.String());
            DropColumn("dbo.Students", "RegistrationAccessCode");
        }
    }
}
