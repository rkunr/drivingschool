namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewViewModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "PasscodeId", c => c.Int(nullable: false));
            AlterColumn("dbo.Students", "PassCode", c => c.String());
            CreateIndex("dbo.Students", "PasscodeId");
            AddForeignKey("dbo.Students", "PasscodeId", "dbo.Passcodes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "PasscodeId", "dbo.Passcodes");
            DropIndex("dbo.Students", new[] { "PasscodeId" });
            AlterColumn("dbo.Students", "PassCode", c => c.String(nullable: false));
            DropColumn("dbo.Students", "PasscodeId");
        }
    }
}
