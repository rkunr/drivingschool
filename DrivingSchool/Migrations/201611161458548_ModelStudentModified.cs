namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelStudentModified : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "StuStatus", c => c.String());
            DropColumn("dbo.Students", "StudentStatus");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Students", "StudentStatus", c => c.String());
            DropColumn("dbo.Students", "StuStatus");
        }
    }
}
