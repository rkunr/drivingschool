namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MadeMobileNumberNullableInRequestAMeeting : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RequestAMeetings", "MobileNumber", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RequestAMeetings", "MobileNumber", c => c.Int(nullable: false));
        }
    }
}
