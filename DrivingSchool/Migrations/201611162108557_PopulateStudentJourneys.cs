using System.Web.UI.WebControls;

namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateStudentJourneys : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO StudentJourneys(Status) VALUES ('New')");
            Sql("INSERT INTO StudentJourneys(Status) VALUES ('Theory')");
            Sql("INSERT INTO StudentJourneys(Status) VALUES ('Practical')");
            Sql("INSERT INTO StudentJourneys(Status) VALUES ('Incomplete')");
        }
        
        public override void Down()
        {
        }
    }
}
