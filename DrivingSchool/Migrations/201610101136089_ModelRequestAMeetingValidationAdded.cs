namespace DrivingSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelRequestAMeetingValidationAdded : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RequestAMeetings", "Regarding", c => c.String(nullable: false));
            AlterColumn("dbo.RequestAMeetings", "FullName", c => c.String(nullable: false));
            AlterColumn("dbo.RequestAMeetings", "EmailAddress", c => c.String(nullable: false));
            AlterColumn("dbo.RequestAMeetings", "Message", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RequestAMeetings", "Message", c => c.String());
            AlterColumn("dbo.RequestAMeetings", "EmailAddress", c => c.String());
            AlterColumn("dbo.RequestAMeetings", "FullName", c => c.String());
            AlterColumn("dbo.RequestAMeetings", "Regarding", c => c.String());
        }
    }
}
