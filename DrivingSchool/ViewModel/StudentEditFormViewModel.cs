﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DrivingSchool.Models;

namespace DrivingSchool.ViewModel
{
    public class StudentEditFormViewModel
    {
        public IEnumerable<StudentJourney> StudentJourneys { get; set; }
        public Student Student { get; set; }
    }
}