﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DrivingSchool.Models;

namespace DrivingSchool.ViewModel
{
    public class RequestAMeetingFormViewModel
    {
        public IEnumerable<RequestAMeetingFor> RequestAMeetingFors { get; set; }
        public RequestAMeeting RequestAMeeting { get; set; }
       
    }
}