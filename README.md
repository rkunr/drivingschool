DrivingSchool
As I have not yet implemented Identity/Role for the admin due to the lack of scope (if it's gonna be one user admin or more user will administer). I shall implement accordingly.

To View the Admin Dashboard, please use the following url:
http://localhost:4475/Home/Admin where port_number will/can be different

The landing page or home page could be viewed using running the program. No need to login to view the Admin page yet as I have not implemented the Role.